class UserBlock::User < ApplicationRecord
  has_secure_password
  has_one_attached :image

  validates  :role,:password, presence: true
  validates :email,:mobile_number, presence: true

  enum role: ["admin", "customer"]
  enum status: ["activate", "deactivate"]

  has_many :banks, class_name: 'BankBlock::Bank', foreign_key: 'user_id'
  has_one :card, class_name: 'CardBlock::Card', through: :bank

end
