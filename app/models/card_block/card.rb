class CardBlock::Card < ApplicationRecord
	belongs_to :user, class_name: 'UserBlock::User', foreign_key: 'user_id'
  belongs_to :bank, class_name: 'BankBlock::Bank', foreign_key: 'bank_id'
end

