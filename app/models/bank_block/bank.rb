class BankBlock::Bank < ApplicationRecord
	belongs_to :user, class_name:'UserBlock::User'
	has_one :card, class_name:'CardBlock::Card'
end
