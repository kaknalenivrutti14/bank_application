class BankBlock::BankSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id,:name,:address,:account_number,:balance,:account_type,:ifsc_code
  attribute :user_details do |object|
    {
      full_name: object.user.first_name + object.user.last_name
    }
  end
end
