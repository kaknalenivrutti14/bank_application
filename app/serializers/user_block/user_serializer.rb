class UserBlock::UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id,:user_name, :first_name,:last_name,:email,:mobile_number,:password,:role
end
