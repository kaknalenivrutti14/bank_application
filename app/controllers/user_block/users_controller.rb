class UserBlock::UsersController < ApplicationController
	skip_before_action :authentication

	def sign_up
		user = UserBlock::User.new(user_params)
		if user_params[:password] == params[:confirm_password]
			if user.save
				token = JwtToken.encode_data(user:user.id)
				render json:{user: UserBlock::UserSerializer.new(user),token:token, message:"user is created"}, status: 201
			else
				render json:{error:user.errors.full_messages,message:"user not created"}, status: 422
			end
		else
			render json:{ message: "Password does not match" }, status: 422
		end
	end

	def login
		user = UserBlock::User.find_by('user_name = ? OR email= ?',params[:user_name],params[:email])
    if user && user.authenticate(params[:password])
		  token = JwtToken.encode_data(user:user.id)
			render json:{user:UserBlock::UserSerializer.new(user),token:token, message:"user login sucessfully" }, status: 200
		else
			render json:{ message:"please enter valid credentials" }, status: 422
		end
	end

	private
	def user_params
		params.permit(:user_name, :first_name,:last_name,:email,:mobile_number,:password,:role,:confirm_password,:image)
	end
end
