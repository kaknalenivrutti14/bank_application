class ApplicationController < ActionController::Base
	before_action :authentication
	skip_forgery_protection

  def authentication
    token = request.headers['token']
    if token
      decode_data = JwtToken.decode_data(token)
      @current_user = User.find(decode_data[0]["user"])
    end
  end
end
