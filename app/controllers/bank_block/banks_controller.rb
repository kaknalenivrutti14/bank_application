class BankBlock::BanksController < ApplicationController
	before_action :current_user,expect:[:all_banks]
	def create
		bank = @current_user.banks.build(bank_params)
		if bank.save
			render json:{message:'bank added sucessfully',bank:BankBlock::BankSerializer.new(bank).serializable_hash,status: true},status:201
		else
			render json:{error:bank.error.full_message,status: false},status: :422
		end
	end
	def index
		render json:{all_banks:"#{@current_user.banks}",status:true},status:200
	end

	def update 
		@bank = @current_user.banks.find(params[:bank_id])
		if @bank
			@bank.update(bank_params)
			render json:{message:'bank updated sucessfully',status:true},status:200
		else 
			render json:{error:"bank id #{params[:bank_id]} is not a bank of #{@current_user.name}",status:true},status:422
		end
	end
	def destory
		@bank.destory
		render json:{message:"#{@bank.name} is removed sucessfully",status:true},status:200
	end
	def all_banks
		render json:{all_banks:"#{BankBlock::Bank.all}",status:true},status:200
	end
	private 
	def bank_params
		params.require(:banks).permit(:name,:address,:account_number,:balance,:account_type,:ifsc_code)
	end

	def get_bank
		@bank = @current_user.banks.find(params[:bank_id])
	end
end
