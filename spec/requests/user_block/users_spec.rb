require 'rails_helper'

RSpec.describe "UserBlock::Users", type: :request do
  before(:each) do
    @user = FactoryBot.create(:user_block_user)
    @token = JwtToken.encode_data(user:@user.id)

    @valid_attribute = {
      first_name:"nivrutti", user_name: "nivruttikaknale",
      email: "nivrutti@gmail.com", 
      mobile_number:"9350003661",
      password:"Nivrutti@123",
      role: "admin",
      status: "activate",
      confirm_password: "Nivrutti@123"
    }

    @invalid_password = {
      first_name:"nivrutti", user_name: "nivruttikaknale",
      email: "nivrutti@gmail.com", 
      mobile_number:"9350003661",
      password:"Nivrutti@123",
      role: "admin",
      status: "activate",
      confirm_password: "Nivrutti@1234"
    }
    @invalid_attribute = {
      first_name:"nivrutti", user_name: "nivruttikaknale",
      email: "nivrutti@gmail.com", 
      password:"Nivrutti@123",
      confirm_password: "Nivrutti@123"
    }
    
    @login_attribute = {
      email: "nivrutti@gmail.com", 
      password:"Nivrutti@123"
    }
    @invalid_creads = {
      email:"nivrutti@test",
      password:"11111"
    }
  end

  describe "POST /sign_up" do
    it "should return 200:OK" do
      post "/user_block/sign_up", params: @valid_attribute
       expect(response).to have_http_status(201)
       expect(JSON.parse(response.body)['message']).to eq("user is created")
    end
     it "should return 422:OK" do
      post "/user_block/sign_up", params: @invalid_attribute
      expect(response).to have_http_status(422)
      expect(JSON.parse(response.body)['message']).to eq("user not created")
    end

     it "Category name has already been taken" do
      post "/user_block/sign_up", params:@invalid_password
      expect(response).to have_http_status(422)
      expect(JSON.parse(response.body)['message']).to eq("Password does not match")
    end   
  end

  describe "POST /sign_up" do
    it "should return 200:OK" do
      post "/user_block/login", params: @login_attribute
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['message']).to eq("user login sucessfully")
    end

    it "should return 200:OK" do
      post "/user_block/login", params: @invalid_creads
      expect(response).to have_http_status(422)
      expect(JSON.parse(response.body)['message']).to eq("please enter valid credentials")
    end
  end
end
