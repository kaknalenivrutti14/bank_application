FactoryBot.define do
  factory :card_block_card, class: 'CardBlock::Card' do
    user_name { "MyString" }
    card_number { "MyString" }
    expire_date { "2024-04-05" }
    cvv_number { 1 }
    card_type { 1 }
  end
end
