FactoryBot.define do
  factory :bank_block_bank, class: 'BankBlock::Bank' do
    name { "MyString" }
    address { "MyString" }
    account_number { "MyString" }
    balance { 1 }
    account_type { 1 }
    ifsc_code { "MyString" }
  end
end
