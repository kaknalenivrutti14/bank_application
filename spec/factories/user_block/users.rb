FactoryBot.define do
  factory :user_block_user, class: 'UserBlock::User' do
    first_name { "nivrutti" }
    last_name { "kaknale" }
    email { "nivrutti@gmail.com" }
    mobile_number { "9359003661" }
    password { "Nivrutti@123" }
    user_name { "nivruttikaknale" }
    status { 0 }
    role { 0 }
  end
end
