class CreateBankBlockBanks < ActiveRecord::Migration[7.1]
  def change
    create_table :banks do |t|
      t.string :name
      t.string :address
      t.string :account_number
      t.integer :balance
      t.integer :account_type
      t.string :ifsc_code
      t.belongs_to :user,foreign_key: true
      t.timestamps
    end
  end
end
