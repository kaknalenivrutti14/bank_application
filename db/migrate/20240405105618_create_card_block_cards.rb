class CreateCardBlockCards < ActiveRecord::Migration[7.1]
  def change
    create_table :cards do |t|
      t.string :user_name
      t.string :card_number
      t.date :expire_date
      t.integer :cvv_number
      t.integer :card_type
      t.belongs_to :user
      t.belongs_to :bank
      t.timestamps
    end
  end
end
