class CreateUserBlockUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :mobile_number
      t.string :password_digest
      t.string :user_name
      t.integer :status
      t.integer :role

      t.timestamps
    end
  end
end
