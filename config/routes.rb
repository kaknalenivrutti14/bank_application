Rails.application.routes.draw do
  get "up" => "rails/health#show", as: :rails_health_check

  namespace :user_block do 
    post 'sign_up',to:'users#sign_up'
    post 'login',to:'users#login'
    resources :users
  end
end
